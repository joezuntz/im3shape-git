import numpy as np
import math
import sys

from .common import *
from . import structs
from . import lib

def make_method_from_c_function(name, funcptr):
    def method(self, *args):
        return funcptr(self, *args)
    method.func_name = name
    return method

class I3ObjectMeta(type):
    def __new__(cls, name, bases, dct):
        for item_name, item in dct.items():
            if isinstance(item,ct._CFuncPtr):
                dct[item_name] = make_method_from_c_function(item_name, item)
        return type.__new__(cls, name, bases, dct)

class I3Object(object):
    __metaclass__ = I3ObjectMeta

    @property
    def _as_parameter_(self):
        return self._struct

    def __getattr__(self, attr):
        return getattr(self._struct[0], attr)





