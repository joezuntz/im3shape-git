import sys
from .common import *
from . import structs
from . import lib
from .i3object import I3Object
from .image import Image
from . import utils



class Dataset(I3Object):
    def __init__(self, options, image, psf, weight=None, ID=None):
        if weight is None: 
            weight = options.noise_sigma ** -2
        if isinstance(weight, float):
            weight_image = Image(image.nx, image.ny)
            weight_image.fill(weight)
            weight = weight_image
        if ID is None:
            ID = utils.ID_from_timestamp()
        self.weight = weight #This needs to be here otherwise weight gets GC'd
        self._struct = lib.i3_build_dataset_with_psf_image(options, ID, image, weight, psf)



