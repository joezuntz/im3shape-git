import numpy.linalg
from analyze_meds import I3Analysis
from .i3meds import I3MultiExposureInputs, I3MEDS
from .output import TextOutput, DatabaseOutput
from .multi_epoch_gal import dithered_gal
from .options import Options
from . import structs
import sys
import galsim
import os
import numpy as np
import traceback
from collections import OrderedDict
from copy import copy


MOFFAT_MODE = 1
DES_MODE = 2
DES_MOFFAT_MODE =3

class NBCObject(object):
    def __init__(self, options):
        self.options = options

class PSFInfo(NBCObject):
    """docstring for PSFInfo"""
    def __init__(self, options, index, e1, e2, beta, fwhm):
        super(PSFInfo, self).__init__(options)
        self.index = index
        self.e1 = e1
        self.e2 = e2
        self.beta = beta
        self.fwhm = fwhm

    def __str__(self):
        return "[{self.index}]  beta={self.beta}  fwhm={self.fwhm}  e1={self.e1}  e2={self.e2}".format(self=self)

    def generate_profile(self):
        moffat = galsim.Moffat(beta=self.beta, fwhm=self.fwhm)
        moffat = moffat.shear(g1=self.e1, g2=self.e2)
        return moffat

    def generate(self):
        upsampling = self.options['upsampling']
        padding = self.options['padding']
        pixel_scale = self.options['nbc_pixel_scale']
        stamp_size = self.options['stamp_size']
        n = self.options['nbc_exposures']
        psf_image_size = upsampling*(stamp_size + padding)
        psfs = []
        psf_profiles = []
        for i in xrange(n):
            psf_profile = self.generate_profile()
            pix = galsim.Pixel(scale=pixel_scale)
            psf_profile = galsim.Convolve([psf_profile,pix])
            psf_image = galsim.ImageF(psf_image_size, psf_image_size)
            psf_profile.drawImage(psf_image, scale = pixel_scale/upsampling, method='no_pixel')
            psf_image = psf_image.array
            psf_profiles.append(psf_profile)
            psfs.append(psf_image)
        return psfs, psf_profiles

class KolmogorovPSFInfo(PSFInfo):
    def generate_profile(self):
        kolmogorov = galsim.Kolmogorov(fwhm=self.fwhm)
        kolmogorov = kolmogorov.shear(g1=self.e1, g2=self.e2)
        return kolmogorov


class IntrinsicInfo(NBCObject):
    """docstring for IntrinsicInfo"""
    def __init__(self, options, index, e1, e2, rotation, hlr, snr):
        super(IntrinsicInfo, self).__init__(options)
        self.e1 = e1
        self.e2 = e2
        self.hlr = hlr
        self.snr = snr
        self.index = index
        self.rotation = rotation

    def __str__(self):
        return "[{self.index}]  e1={self.e1}  e2={self.e2}  hlr={self.hlr}  snr={self.snr}  rot={self.rotation}".format(self=self)

    def generate(self):
        galaxy = galsim.Exponential(flux=1.0, half_light_radius=self.hlr)
        galaxy = galaxy.shear(g1=self.e1, g2=self.e2)
        galaxy = galaxy.rotate(self.rotation*galsim.radians)
        return galaxy


class GaussianIntrinsicInfo(IntrinsicInfo):
    def generate(self):
        galaxy = galsim.Gaussian(flux=1.0, half_light_radius=self.hlr)
        galaxy = galaxy.shear(g1=self.e1, g2=self.e2)
        galaxy = galaxy.rotate(self.rotation*galsim.radians)
        return galaxy

class RealIntrinsicInfo(IntrinsicInfo):
    def __init__(self, options, index, e1, e2, rotation, hlr, snr, real_galaxy_index):
        super(RealIntrinsicInfo,self).__init__(options, index, e1, e2, rotation, hlr, snr)
        self.real_galaxy_index = real_galaxy_index
        self.load()

    def load(self):
        cat=galsim.RealGalaxyCatalog("COSMOS_23.5_training_sample/real_galaxy_catalog_23.5.fits")
        self.base_real_galaxy_info=galsim.RealGalaxy(cat,self.real_galaxy_index)

    def set_hlr(self, hlr):
        #measure the size of the current galaxy by convolving with small PSF and measuring
        psf = galsim.Gaussian(fwhm=0.1)
        scale = 0.01
        src=galsim.Convolve([self.base_real_galaxy_info,psf])
        hsm=galsim.hsm.FindAdaptiveMom(src.drawImage(scale=scale))
        sigma = hsm.moments_sigma*scale
        current_hlr = np.sqrt(-2*np.log(0.5))*sigma
        self.real_galaxy_info = self.base_real_galaxy_info.magnify(hlr/current_hlr)

    def reset(self):
        del self.base_real_galaxy_info
        del self.real_galaxy_info

    def generate(self):
        if not hasattr(self, "base_real_galaxy_info"):
            self.load()
        galaxy = self.real_galaxy_info
        #intrinsic ellipticity
        galaxy = galaxy.shear(g1=self.e1, g2=self.e2)
        galaxy = galaxy.rotate(self.rotation*galsim.radians)
        return galaxy

class ShearInfo(NBCObject):
    """docstring for ShearInfo"""
    def __init__(self, options, index, g1, g2):
        super(ShearInfo, self).__init__(options)
        self.g1 = g1
        self.g2 = g2
        self.index = index
    def __str__(self):
        return "[{self.index}]  g1={self.g1}  g2={self.g2}".format(self=self)


class RunInfo(NBCObject):
    """docstring for RunInfo"""
    def __init__(self, options, rng, intrinsic, psf, shear, realisation):
        super(RunInfo, self).__init__(options)
        self.intrinsic = intrinsic
        self.psf = psf
        self.shear = shear
        self.rng = rng
        self.realisation = realisation

    def __str__(self):
        return """Run Info:
    Intrinsic {self.intrinsic}
    PSF {self.psf}
    Shear {self.shear}
        """.format(self=self)
 
    def generate_transform_from_offset(self, x0,  y0):
        pixel_scale = self.options['nbc_pixel_scale']
        stamp_size = self.options['stamp_size']
        # Build the transform from sky to image coordinates
        transform = structs.Transform_struct()
        transform.ra0 = 0.0  # these are not used in this case
        transform.dec0 = 0.0
        transform.cosdec0 = 0.0 
        transform.x0 = x0/pixel_scale + stamp_size/2.0
        transform.y0 = y0/pixel_scale + stamp_size/2.0

        transform.A[0][0] = 1.0/pixel_scale
        transform.A[0][1] = 0.0
        transform.A[1][0] = 0.0
        transform.A[1][1] = 1.0/pixel_scale

        return transform

    def metadata(self):
        return OrderedDict(
            true_g1 = self.shear.g1, 
            true_g2 = self.shear.g2 ,
            true_unrotated_intrinsic_e1 = self.intrinsic.e1,
            true_unrotated_intrinsic_e2 = self.intrinsic.e2,
            true_half_light_radius = self.intrinsic.hlr, 
            true_snr = self.intrinsic.snr,
            true_beta = self.psf.beta,
            true_psf_fwhm = self.psf.fwhm,
            true_psf_e1 = self.psf.e1, 
            true_psf_e2 = self.psf.e2,
            true_rotation = self.intrinsic.rotation,
            true_intrinsic_index = self.intrinsic.index,
            true_g_index = self.shear.index,
            true_psf_index = self.psf.index,
            stamp_size = self.options['stamp_size'],
            bands = None,
            realisation = self.realisation,
            n_exposures = self.options['nbc_exposures'],
            n_rotations = self.options['nbc_rotations'],
            )


    def generate(self):
        n_exposures = self.options['nbc_exposures']
        scale_offset = self.options['nbc_offset_scale']
        stamp_size = self.options['stamp_size']
        pixel_scale = self.options['nbc_pixel_scale']
        galaxy = self.intrinsic.generate()
        galaxy = galaxy.shear(g1=self.shear.g1, g2=self.shear.g2)

        psfs, psf_profiles = self.psf.generate()

        d=dithered_gal(galaxy, psf_profiles, self.intrinsic.snr, n_exposures, self.rng, pixel_scale, stamp_size)
        images,dxs,dys=d.get_images_and_transforms(scale_offset=scale_offset)

        inputs = I3MultiExposureInputs()
        for image, dx, dy, psf in zip(images, dxs, dys, psfs):
            weight = np.ones_like(image) / d.noise_sigma_per_exposure**2

            #transforms should be in pixels, based on what is in I3MEDS
            transform = self.generate_transform_from_offset(dx, dy)
            info = {
                'image' : image,
                'weight' : weight,
                'psf' : psf,
                'uber_mask' : None,
                'stamp_size' : stamp_size,
                'transform':transform,
            }
            inputs.append(info)
        inputs.meta.update(self.metadata())

        return inputs

import os
import mpi4py.MPI
rank = mpi4py.MPI.COMM_WORLD.Get_rank()
shared_rng = galsim.BaseDeviate(2345+rank)


class RingTest(NBCObject):
    """docstring for RingTest"""
    def __init__(self, options, intrinsic, psf, realization_start=0):
        super(RingTest, self).__init__(options)
        self.intrinsic = intrinsic
        self.psf = psf
        self.shears = [(0.0, 0.0), (0.05, 0.05)]
        self.seed = self.options['nbc_seed']
        number_angles = self.options['nbc_rotations']
        self.realizations = self.options['nbc_realizations']
        self.rotation_angles = np.arange(number_angles)*2*np.pi/number_angles
        self.realization_start = realization_start

    def generate(self):
        #yield a series of RunInfo objects
        #make a new rotation object for each run
        for i,(g1,g2) in enumerate(self.shears):
            #we use the same RNG for each shear and for each PSF
            #so the noise realization only changes with realization
            #and intrinsic rotation changes
            #rng = galsim.BaseDeviate(self.seed)
            print "Warning RNG shared"
            rng = shared_rng
            for r,rotation in enumerate(self.rotation_angles):
                intrinsic = copy(self.intrinsic)
                intrinsic.rotation = rotation
                intrinsic.index = r
                for realization in xrange(self.realizations):
                    shear = ShearInfo(self.options, i, g1, g2)
                    run = RunInfo(self.options, rng, intrinsic, self.psf, shear, self.realization_start+realization)
                    yield run

class PsfLeakageTest(NBCObject):
    def __init__(self, options, intrinsic, psf):
        super(PsfLeakageTest, self).__init__(options)
        self.intrinsic = intrinsic
        self.psf = psf
        self.psf_e_values = [(0.0, 0.0)] #, (0.1, 0.1)]

    def generate(self):
        for p,(e1,e2) in enumerate(self.psf_e_values):
            psf = copy(self.psf)
            psf.index = p
            psf.e1 = e1
            psf.e2 = e2
            ring = RingTest(self.options, self.intrinsic, psf)
            for run in ring.generate():
                yield run




def load_psfs(options, coadd_objects_ids):
    """ Not working yet """
    import desdb
    filename = "DES0111-4914-r-meds-013.fits.fz"
    print "Hardcoded MEDS filename: ", filename
    m=I3MEDS(filename)
    images = []
    profiles = []
    e_values = []
    conn=desdb.Connection()
    for coadd_objects_id in coadd_objects_ids:
        #open meds file and load bundled PSF
        iobj = np.where(m['id']==coadd_objects_id)[0][0]
        psf_object, psf_image = m.get_bundled_psfex_psf(iobj, 1, options, return_profile=True)
        images.append(psf_image)
        r=conn.quick("select psf_e1, psf_e2  from im3shape_v7_epoch_r where coadd_objects_id=%d and exposure_index=1"%coadd_objects_id)
        images.append(psf_image)
        # orig_col = m['orig_col'][iobj][1]
        # orig_row = m['orig_row'][iobj][1]
        # pos = galsim.PositionD(orig_col, orig_row)
        # psf_object = psf_profile.getPSF(pos)
        profiles.append(psf_object)
        e_values.append((r[0]['psf_e1'], r[0]['psf_e2']))
    print coadd_objects_ids, e_values
    return images, profiles, e_values

FINALIZED="FINALIZED"

class RingTestAnalysisSlaveOutput(object):
    def __init__(self, comm, base_name):
        self.comm=comm
        self.rank=comm.Get_rank()

    def write_row(self, main, epochs,tag=None):
        if tag is not None:
            self.comm.send([main, epochs], dest=0, tag=tag)
        else:
            self.comm.send([main, epochs], dest=0, tag=self.rank)

    def finalize(self, tag=None):
        print "process %d finalizing with tag %d"%(self.rank,tag)
        if tag is not None:
            self.comm.send(FINALIZED, dest=0, tag=tag)        
        else:
            self.comm.send(FINALIZED, dest=0, tag=self.rank)

class RingTestAnalysisMasterOutput(object):
    def __init__(self, comm, base_name):
        self.comm=comm
        self.rank=comm.Get_rank()
        self.size=comm.Get_size()
        quantities = ['true_g','true_psf_e','e']
        self.measurements = []
        self.base_name = base_name
        for q in quantities:
            self.measurements.append(q+'1')
            self.measurements.append(q+'2')
        self.measurements.append('true_rotation')
        self.measurements.append('mean_rgpp_rp')
        self.measurements.append('snr')
        self.measurements.append('true_psf_index')
        self.measurements.append('true_g_index')
        self.measurements.append('realisation')
        self.measurements.append('n_exposures')
        self.measurements.append('true_half_light_radius')
        self.measurements.append('true_psf_fwhm')
        self.measurements.append('n_rotations')
        self.columns = ['rgpp_rp', 'rgpp_rp_std', 'snr', 'snr_std', 'm1', 'm2', 'm1_err', 'm2_err', 'c1', 'c2', 
                        'c1_err', 'c2_err', 'alpha1', 'alpha2', 'alpha12', 'alpha21', 'b1', 'b2','true_hlr',
                        'true_psf_fwhm','n_exp','n_rot']
        self.file = open(base_name+'.ring_results.txt', 'w')
        self.file.write('#')
        for col in self.columns:
            self.file.write(col+"    ")
        self.file.write('\n')
        self.reset()

    def reset(self):
        self.true_g1 = []
        self.true_g2 = []
        self.true_psf_e1 = []
        self.true_psf_e2 = []
        self.measured_e1 = []
        self.measured_e2 = []
        self.rotation = []
        self.realisation = []
        for m in self.measurements:
            setattr(self, m, list())

    def save_result(self, main):
        for m in self.measurements:
            v = getattr(self, m)
            v.append(main[m])

    def master_loop(self, tag=None, n_res=0):
        n=0
        import mpi4py.MPI
        while n < n_res: #self.size-1:
            status = mpi4py.MPI.Status()
            if tag is not None:
                results = self.comm.recv(source=mpi4py.MPI.ANY_SOURCE, tag=tag, status=status)
            else:
                results = self.comm.recv(source=mpi4py.MPI.ANY_SOURCE, tag=mpi4py.MPI.ANY_TAG, status=status)
            if results=="FINALIZED":
                print "Processor %d finalized" % (status.source)
            #    n+=1
                continue
            n+=1
            main,epoch=results
            print "Result %d receieved from %d" %(main['iobj'], main['processor'])
            self.save_result(main)
        self.finalize()

    def finalize(self):
        for m in self.measurements:
            setattr(self, m, np.array(getattr(self, m)))
        realisation_nums=np.unique(self.realisation)
        psf_indices = np.unique(self.true_psf_index)
        g_indices = np.unique(self.true_g_index)

        m1 = np.zeros(len(psf_indices))
        m2 = np.zeros(len(psf_indices))
        m1_err = np.zeros(len(psf_indices))
        m2_err = np.zeros(len(psf_indices))
        c1 = np.zeros(len(psf_indices))
        c2 = np.zeros(len(psf_indices))
        c1_err = np.zeros(len(psf_indices))
        c2_err = np.zeros(len(psf_indices))
        psf_e1 = np.zeros(len(psf_indices))
        psf_e2 = np.zeros(len(psf_indices))
        outputs = {}

        for p_i in psf_indices:
            ring_measured_g1s=[]
            ring_measured_g2s=[]
            ring_true_g1s=[]
            ring_true_g2s=[]
            for r in realisation_nums:

                measured_g1 = np.zeros(len(g_indices))
                measured_g2 = np.zeros(len(g_indices))
                true_g1 = np.zeros(len(g_indices))
                true_g2 = np.zeros(len(g_indices))

                for g_i in g_indices:
                    w = np.where((self.realisation==r) & (self.true_g_index==g_i) & (self.true_psf_index==p_i))[0]
                    #w should have length n_rotations, if it doesn't, skip this realisation
                    if len(w) != self.n_rotations[0]:
                        print 'r=%d, g_i=%d, p_i=%d, only %d rotations, skipping'%(r,g_i,p_i,len(w))
                        print 'w:',w
                        continue
                    ring_measured_g1s.append(self.e1[w].mean())
                    ring_measured_g2s.append(self.e2[w].mean())
                    ring_true_g1s.append(self.true_g1[w[0]])
                    ring_true_g2s.append(self.true_g2[w[0]])
                    true_psf_e1 = self.true_psf_e1[w[0]]
                    true_psf_e2 = self.true_psf_e2[w[0]]                    

            print 'true_g1s:',ring_true_g1s
            print 'measured_g1s:',ring_measured_g1s
            ps1, cov1 = np.polyfit(ring_true_g1s,ring_measured_g1s, 1, cov=True)
            m1[p_i], c1[p_i] = ps1[0], ps1[1] 
            m1_err[p_i] = np.sqrt(cov1[0,0])
            c1_err[p_i] = np.sqrt(cov1[1,1])
            ps2, cov2= np.polyfit(ring_true_g2s,ring_measured_g2s, 1, cov=True)
            m2[p_i], c2[p_i] = ps2[0], ps2[1] 
            m2_err[p_i] = np.sqrt(cov2[0,0])
            c2_err[p_i] = np.sqrt(cov2[1,1])

            psf_e1[p_i] = true_psf_e1
            psf_e2[p_i] = true_psf_e2

        alpha1, b1 = 0,0 # np.polyfit(psf_e1, c1, 1) 
        alpha2, b2 = 0,0 #np.polyfit(psf_e2, c2, 1)
        alpha12, b12 = 0,0 #np.polyfit(psf_e1, c2, 1) 
        alpha21, b21 = 0,0 #np.polyfit(psf_e2, c1, 1)

        ring_file = self.base_name+".ring.txt"
        np.savetxt(ring_file, np.transpose([self.true_g1, self.true_g2, self.e1, self.e2, self.true_psf_index, self.true_g_index]))
        outputs['m1'] = m1.mean()
        outputs['m2'] = m2.mean()
        outputs['m1_err'] = m1_err.mean()
        outputs['m2_err'] = m2_err.mean()
        outputs['c1'] = c1.mean()
        outputs['c2'] = c2.mean()
        outputs['c1_err'] = c1_err.mean()
        outputs['c2_err'] = c2_err.mean()
        outputs['snr'] = self.snr.mean()
        outputs['snr_std'] = self.snr.std()
        outputs['rgpp_rp'] = self.mean_rgpp_rp.mean()
        outputs['rgpp_rp_std'] = self.mean_rgpp_rp.std()
        outputs['alpha1'] = alpha1
        outputs['alpha2'] = alpha2
        outputs['alpha12'] = alpha12
        outputs['alpha21'] = alpha21
        outputs['b1'] = b1
        outputs['b2'] = b2
        print 'self.true_half_light_radius',self.true_half_light_radius
        outputs['true_hlr'] = self.true_half_light_radius[0]
        outputs['true_psf_fwhm'] = self.true_psf_fwhm[0]
        outputs['n_exp'] = self.n_exposures[0]
        outputs['n_rot'] = self.n_rotations[0]
        #analysis goes here
        print "alpha1 = ", alpha1
        print "alpha2 = ", alpha2
        print "alpha12 = ", alpha12
        print "alpha21 = ", alpha21

        for col in self.columns:
            self.file.write('%s    '%outputs[col])
        self.file.write('\n')
        self.file.flush()
        self.reset()

class I3GalsimAnalysis(I3Analysis):
    def __init__(self, args):
        options=Options(args.ini)
        super(I3GalsimAnalysis, self).__init__(options)

    def select_output(self, output, main_header, epoch_header, comm):
        if comm is not None:
            if comm.Get_rank()==0:
                output = RingTestAnalysisMasterOutput(comm, output)
            else:
                output = RingTestAnalysisSlaveOutput(comm, output)
        elif self.options.database_output:
            if isinstance(output, DatabaseOutput):
                output = output
            else:
                output = DatabaseOutput(output)
        else:
            output = TextOutput(output+".main.txt", output+".epoch.txt",main_header, epoch_header)
        return output


    def main(self, run_generator, output, fatal_errors, comm, tag=None):
        self.options.validate()
        if comm is None:
            rank=0
            size=1
        else:            
            #The master doesn't do any work
            rank = comm.Get_rank() - 1
            size = comm.Get_size() - 1

        for i,run in enumerate(run_generator.generate()):
            if i%size!=rank: continue
            info = run.generate()
            info.meta['iobj'] = i
            info.meta['row_id'] = i
            info.meta['ID'] = i
            info.meta['processor'] = rank
            try:
                self.check_valid(info)
                extra = self.preprocess(info, self.options)
                result = self.run(info, self.options)
                main, epoch = self.postprocess(result, info)
                main.update(extra)
                print "Best fit: e1=%.3e  e2=%.3e"%(main['e1'], main['e2'])
                output.write_row(main, epoch, tag=tag)
            except KeyboardInterrupt:
                raise
            except Exception as error:
                exc_type, exc_obj, exc_tb = sys.exc_info()
                fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
                traceback.print_exc()
                if fatal_errors:
                    raise
        output.finalize(tag=tag)

    def preprocess(self, info, options):
        extra = {}
        extra.update(info.meta)
        return extra

    def postprocess_epoch(self, fitInfo, epoch_results, epoch_info):
        super(I3GalsimAnalysis, self).postprocess_epoch(fitInfo, epoch_results, epoch_info)
        epoch_results["hsm_psf_e1_sky"] = epoch_results["hsm_psf_e1"]
        epoch_results["hsm_psf_e2_sky"] = epoch_results["hsm_psf_e2"]
        epoch_results["psf_e1_sky"] = epoch_results["psf_e1"]
        epoch_results["psf_e2_sky"] = epoch_results["psf_e2"]

def choose_runs_kolmogorov(options):
    psf = KolmogorovPSFInfo(options, index=0, e1=0.0, e2=0.0, beta=3.0, fwhm=1.0)
    intrinsic = IntrinsicInfo(options, index=0, e1=0.2, e2=0.0, rotation=0.0, hlr=0.3, snr=20.)
    return [PsfLeakageTest(options, intrinsic, psf)]

def choose_runs_moffat(options, args):
    psf = PSFInfo(options, index=0, e1=0.0, e2=0.0, beta=3.0, fwhm=1.0)
    intrinsic = IntrinsicInfo(options, index=0, e1=0.2, e2=0.0, rotation=0.0, hlr=args.hlr, snr=args.snr)
    return [PsfLeakageTest(options, intrinsic, psf)]

def choose_runs_ring(options, args):
    psf = PSFInfo(options, index=0, e1=0.0, e2=0.0, beta=3.0, fwhm=args.psf_fwhm)
    intrinsic = IntrinsicInfo(options, index=0, e1=0.2, e2=0.0, rotation=0.0, hlr=args.hlr, snr=args.snr)
    return [RingTest(options, intrinsic, psf)]


"""
def choose_runs_grid(options):
    for fwhm in np.linspace(0.4, 1.2, 10)[::-1]:
        for snr in np.logspace(1.0, 2.0, 10):
            psf = KolmogorovPSFInfo(options, index=0, e1=0.0, e2=0.0, beta=3.0, fwhm=fwhm)
            intrinsic = IntrinsicInfo(options, index=0, e1=0.2, e2=0.0, rotation=0.0, hlr=0.3, snr=snr)
            # intrinsic = RealIntrinsicInfo(options, index=0, e1=0.2, e2=0.0, rotation=0.0, hlr=0.3, 
            #     snr=snr, real_galaxy_index=options.nbc_real_galaxy_index)
            # intrinsic.set_hlr(0.3)
            yield PsfLeakageTest(options, intrinsic, psf)
"""
def choose_runs_grid(options, args):
    grid_point=0
    for psf_fwhm in [0.5,0.9]:
        for hlr in [0.1, 0.3, 0.6]:
            for n_exp in [1, 4]:
                options["nbc_exposures"]=n_exp
                psf = PSFInfo(options, index=0, e1=0.0, e2=0.0, beta=3.0, fwhm=psf_fwhm)
                intrinsic = IntrinsicInfo(options, index=0, e1=0.2, e2=0.0, rotation=0.0, hlr=hlr, snr=args.snr)
                yield RingTest(options, intrinsic, psf, realization_start=grid_point*options["nbc_realizations"])
                grid_point+=1

choose_runs = choose_runs_grid

def main(args):
    options=Options(args.ini)
    if args.n_exposures is not None:
        options["nbc_exposures"]=args.n_exposures
    if args.n_rotations is not None:
        options["nbc_rotations"]=args.n_rotations
    if args.n_realisations is not None:
        options["nbc_realizations"]=args.n_realisations

    analysis = I3GalsimAnalysis(args)
    if args.mpi:
        import mpi4py.MPI
        comm = mpi4py.MPI.COMM_WORLD
    else:
        comm = None

    output = analysis.select_output(args.output, [], [], comm)
    tag=0
    for generator in choose_runs(options, args):        
        n_res=sum(1 for _ in generator.generate())
        if args.mpi and (comm.Get_rank()==0):
            output.master_loop(tag=tag,n_res=n_res)
        else:
            analysis.main(generator, output, args.fatal_errors, comm, tag=tag)
        tag+=1

import argparse
parser = argparse.ArgumentParser(description="Noise bias calibration", add_help=True)
parser.add_argument("ini", type=str, help="Ini file")
parser.add_argument("output", type=str, help="Output base filename")
parser.add_argument("--n_exposures",type=float,default=None)
parser.add_argument("--n_rotations",type=float,default=None)
parser.add_argument("--n_realisations",type=float,default=None)
parser.add_argument("--snr",type=float,default=1000.)
parser.add_argument("--hlr",type=float,default=0.3)
parser.add_argument("--psf_fwhm",type=float,default=0.9)
parser.add_argument("--fatal-errors", action='store_true', help="Errors are fatal")
parser.add_argument("--scale-offset", type=float, default=1.0, help="Scale of exposure offsets")
parser.add_argument("--verbose", action='store_true', help="Print lots of things out")
parser.add_argument("--mpi", action='store_true', help="Run under MPI")

if __name__ == '__main__':
    args = parser.parse_args()
    main(args)
