from .common import py_i3_flt, py_i3_cpx, c_i3_flt, i3_list_of_models, MAX_STRING_LEN, MAX_N_EXPOSURE
import ctypes as ct
import collections
import sys
from .models import i3_build_options
import importlib

#max number of varied parameters for covariance matrix
MAX_NP2 = 32*32

#######################
# IMAGE STRUCTS
#######################

class I3_Structure(ct.Structure):
    @classmethod
    def named_tuple_from_struct(cls):
        names = [f[0] for f in cls._fields_]
        return collections.namedtuple(cls.__name__+"_tuple", names)
    @classmethod
    def tuple_type(cls):
        return globals()[cls.__name__+"_tuple"]
    def as_tuple(self):
        props = [getattr(self, name[0]) for name in self._fields_]
        return self.tuple_type()(*props)
    def __str__(self):
        parts = ["%s = %r, " % (name, getattr(self, name)) for name,ptype in self._fields_]
        return '\n'.join(parts)

class Image_struct(I3_Structure):
    _fields_ = [("data", ct.POINTER(c_i3_flt)),
                ("row", ct.POINTER(ct.POINTER(c_i3_flt))),
                ("n", ct.c_ulong),
                ("nx", ct.c_ulong),
                ("ny", ct.c_ulong)]

Image_struct_p = ct.POINTER(Image_struct)
Image_struct_pp = ct.POINTER(Image_struct_p)
Image_struct_tuple = Image_struct.named_tuple_from_struct()

#######################
# OPTION SET STRUCTS
#######################


class Options_struct(I3_Structure):
    # Import options from i3_build_options.py for i3_list_of_models
    module = i3_build_options
    option_list = module.generic_options[:]
 
    for model_name in i3_list_of_models:
        option_list.append(module.generate_model_options(model_name, compiling=False))
    option_list += module.experimental_options[:]
    _fields_ = []
    _default_options = {}
    for group_name,option_group in option_list:
        for name,ptype,default,help in option_group:
                
            if ptype == float:
                _fields_.append((name, ct.c_float))                    
            elif ptype == int:
                _fields_.append((name, ct.c_int))
            elif ptype == str:      
                _fields_.append((name, ct.c_char * MAX_STRING_LEN))
            elif ptype == bool:
                _fields_.append((name, ct.c_bool))
            elif ptype == list:
                length = len(default)
                _fields_.append((name, ct.c_float*length))
                default = (ct.c_float*length)(*default)
                del length
            else:
                print "im3shape.py has suffered internal error X14-Q"
                print name, ptype, default,help
                raise ValueError("Please report all this text to the developers")
                        
            _default_options[name] = (ptype, default, help)
    del module, option_list, model_name, group_name, option_group, name, ptype, default, help


Options_struct_p = ct.POINTER(Options_struct)
Options_struct_tuple = Options_struct.named_tuple_from_struct


#######################
# PARAMETER SET STRUCTS
#######################

class MoffatPSF_struct(I3_Structure):
    _fields_ = [("beta", c_i3_flt),                  
                ("fwhm", c_i3_flt),                 
                ("e1", c_i3_flt),
                ("e2", c_i3_flt),              
                ("x", ct.c_int),              
                ("y", ct.c_int)]
    
MoffatPSF_struct_p = ct.POINTER(MoffatPSF_struct)
MoffatPSF_struct_tuple = MoffatPSF_struct.named_tuple_from_struct()

class ParameterSet_struct(I3_Structure):
    def header_line(self):
        texts = []
        for field in self._fields_:
            if issubclass(field[1], ct.Array):
                for i in xrange(field[1]._length_):
                    texts.append('%s-%d'%(field[0],i))
            else:
                texts.append(field[0])
        return '#' + ('    '.join(texts))
    def catalog_line(self):
        texts = []
        for field in self._fields_:
            if issubclass(field[1], ct.Array):
                s=getattr(self, field[0])
                for i in xrange(field[1]._length_):
                    texts.append(str(s[i]))
            else:
                texts.append(str(getattr(self, field[0])))
        return '    '.join(texts)

model_parameter_names = {}

def create_parameter_set_class(model_name):
    class_name = model_name + "_parameter_set"
    fields = []
    module = importlib.import_module('.models.i3_'+model_name,'py3shape')
    global model_parameter_names
    model_parameter_names[model_name] = module.parameters[:]
    for i,name in enumerate(module.parameters):
        ptype = module.types[name]
        if ptype == float:
            fields.append((name, c_i3_flt))
        elif ptype == int:
            fields.append((name, ct.c_int))
        elif ptype == str:
            fields.append((name, ct.c_char*MAX_STRING_LEN))
        elif ptype == bool:
            fields.append((name, ct.c_bool))
        elif ptype == list:
            length = len(module.starts[name])
            fields.append((name, c_i3_flt*length))
    attrs = dict(_fields_=fields,)
    parents = (ParameterSet_struct, )
    return type(class_name, parents, attrs)

model_parameter_names = {}

for model_name in i3_list_of_models:
    locals()[model_name+"_parameter_set"] = create_parameter_set_class(model_name)
    locals()[model_name+"_parameter_set_p"] = ct.POINTER(locals()[model_name+"_parameter_set"])
    locals()[model_name+"_parameter_set_tuple"] = locals()[model_name+"_parameter_set"].named_tuple_from_struct()


#######################
# RESULTS SET STRUCTS
#######################

class Results_struct(I3_Structure):
    _fields_ = [("identifier", ct.c_long),                  
                ("time", c_i3_flt),                 
                ("params", ct.c_void_p),
                ("fluxes", c_i3_flt*2),              
                ("flux_ratio", c_i3_flt),              
                ("snr", c_i3_flt),              
                ("old_snr", c_i3_flt),              
                ("min_residuals", c_i3_flt),            
                ("max_residuals", c_i3_flt),            
                ("model_min", c_i3_flt),                
                ("model_max", c_i3_flt),                
                ("likelihood", c_i3_flt),                 
                ("levmar_start_error", c_i3_flt),         
                ("levmar_end_error", c_i3_flt),       
                ("levmar_resid_grad", c_i3_flt), 
                ("levmar_vector_diff", c_i3_flt),      
                ("levmar_error_diff", c_i3_flt),    
                ("levmar_comp_grad", c_i3_flt),      
                ("levmar_iterations", ct.c_ulong),         
                ("levmar_reason", ct.c_int),        
                ("levmar_like_evals", ct.c_ulong),     
                ("levmar_grad_evals", ct.c_ulong),   
                ("levmar_sys_evals", ct.c_ulong),
                ("exposure_x",c_i3_flt*MAX_N_EXPOSURE),
                ("exposure_y",c_i3_flt*MAX_N_EXPOSURE),
                ("exposure_e1",c_i3_flt*MAX_N_EXPOSURE),
                ("exposure_e2",c_i3_flt*MAX_N_EXPOSURE),
                ("exposure_chi2",c_i3_flt*MAX_N_EXPOSURE),
                ("exposure_residual_stdev",c_i3_flt*MAX_N_EXPOSURE),
                ("exposure_background",c_i3_flt*MAX_N_EXPOSURE),
                ("exposure_band",ct.c_char*MAX_N_EXPOSURE),
                ("mean_flux",c_i3_flt),
                ("number_varied_params", ct.c_int),
                ("covariance_estimate",c_i3_flt*MAX_NP2),
                ]

Results_struct_p = ct.POINTER(Results_struct)
Results_struct_tuple = Results_struct.named_tuple_from_struct()

class Moments_struct(I3_Structure):
    _fields_ = [("sum", c_i3_flt),
                ("x0", c_i3_flt),
                ("y0", c_i3_flt),
                ("qxx", c_i3_flt),
                ("qxy", c_i3_flt),
                ("qyy", c_i3_flt),
                ("e1", c_i3_flt),
                ("e2", c_i3_flt)]

Moments_struct_p = ct.POINTER(Moments_struct)
Moments_struct_tuple = Moments_struct.named_tuple_from_struct()


class Model_struct(I3_Structure):
    _fields_ = [("name", ct.c_char*MAX_STRING_LEN),
                ("nparam", ct.c_int),
                ("param_fixed", ct.POINTER(ct.c_bool)),
                ("likelihood", ct.c_void_p),
                ("prior", ct.c_void_p),
                ("posterior_derivative", ct.c_void_p),
                ("propose", ct.c_void_p),
                ("start", ct.c_void_p),
                ("map_physical", ct.c_void_p),
                ("nbytes", ct.c_int),
                ("param_type", ct.POINTER(ct.c_int)),
                ("byte_offsets", ct.POINTER(ct.c_int)),
                ("param_prior", ct.POINTER(ct.c_void_p)),
                ("param_proposal", ct.POINTER(ct.c_void_p)),
                ("min", ct.c_void_p),
                ("max", ct.c_void_p),
               ]

Model_struct_p = ct.POINTER(Model_struct)
Model_struct_tuple = Model_struct.named_tuple_from_struct()


#do not need to specify all the struct elements - just use pointer everywhere
#we could actually do this for other things but useful to have details.
Dataset_struct_p = ct.c_void_p
ParameterSet_struct_p = ct.c_void_p


### FILE - THIS IS A BIT OF A HACK
class FILE(ct.Structure):
    pass

FILE_p = ct.POINTER(FILE)

PyFile_AsFile = ct.pythonapi.PyFile_AsFile
PyFile_AsFile.restype = FILE_p
PyFile_AsFile.argtypes = [ct.py_object]

c_stdout = PyFile_AsFile(sys.stdout)
c_stderr = PyFile_AsFile(sys.stderr)


class Transform_struct(I3_Structure):
    _fields_ = [
        ("ra0", c_i3_flt),
        ("dec0", c_i3_flt),
        ("cosdec0", c_i3_flt),
        ("x0", c_i3_flt),
        ("y0", c_i3_flt),
        ("A", (c_i3_flt*2)*2),  #non-associative multiplication!  awesome.
    ]
Transform_struct_p = ct.POINTER(Transform_struct)
Transform_struct_tuple = Transform_struct.named_tuple_from_struct()
