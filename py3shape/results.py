import sys
from .common import *
from . import structs
from . import lib
from .i3object import I3Object
from collections import OrderedDict

class Results(I3Object):
    def __init__(self, model_name):
        super(Results, self).__init__()
        class_name = model_name + "_parameter_set"
        self.param_struct_type = getattr(structs, class_name)
        self.param_ptr_type = getattr(structs, class_name+"_p")
        self.obj = structs.Results_struct()
        self.param_struct = self.param_struct_type()
        ptr = ct.pointer(self.param_struct)
        self.obj.params = ct.cast(ptr, ct.c_void_p)
        self._struct = ct.pointer(self.obj)


    def get_params(self):
        return self.param_struct

    def from_dicts(self, main, epoch):
        result = self.as_tuple()
        params = self.get_params()
        n_exposure = main['n_exposure']
        for name, _ in params._fields_:
            setattr(params, name, main[name])

        self.obj.fluxes[0] = main["bulge_flux"]
        self.obj.fluxes[1] = main["bulge_flux"]

        nparam = 0
        while 'covmat_%d_%d'% (nparam,nparam) in main:
            nparam += 1

        for name in result._asdict():
            if name=='params' or name=='fluxes': continue
            elif name.startswith("exposure_"):
                a = getattr(self.obj, name)
                for i in xrange(n_exposure):
                    a[i] = epoch[i][name]
            elif name=='covariance_estimate':
                a = self.obj.covariance_estimate
                for i in xrange(nparam):
                    for j in xrange(nparam):
                        a[i+nparam*j] = main["covmat_%d_%d"%(i,j)]
            else:
                setattr(self.obj, name, value)



    def as_dict(self, n_exposure, nparam):
        result = self.as_tuple()
        main = OrderedDict()
        epoch = [OrderedDict() for i in xrange(n_exposure)]

        params = self.get_params()
        for name, _ in params._fields_:
            main[name] = getattr(params, name)

        for name,value in result._asdict().items():
            if name=="params":continue
            elif name=="fluxes":
                main["bulge_flux"] = value[0]
                main["disc_flux"] = value[1]
            elif name.startswith("exposure_"):
                name = name[9:]
                for i in xrange(n_exposure):
                    epoch[i][name] = value[i]
            elif name=="covariance_estimate":
                for i in xrange(nparam):
                    for j in xrange(nparam):
                        main["covmat_%d_%d"%(i,j)] = value[i+nparam*j]
            else:
                main[name] = value
        return main, epoch
