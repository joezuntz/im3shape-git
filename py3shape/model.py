import sys
from .common import *
from . import structs
from . import lib
from .i3object import I3Object
from .image import Image
import numpy as np

class Model(I3Object):
    def __init__(self, options, name=None):
        if name is None:
            name = options.model_name
        self._struct = lib.i3_model_create(name, options)
        self._param_struct = getattr(structs, name +'_parameter_set')
        self._param_struct_p = getattr(structs, name +'_parameter_set_p')
        self.model_image = Image(options.stamp_size, options.stamp_size)

    def posterior(self, parameters, dataset, model_image=None):
        if model_image is None:
            model_image = self.model_image
        parameters = self.parameter_pointer(parameters)
        return lib.i3_model_posterior(self, model_image, parameters, dataset)

    def likelihood(self, parameters, dataset, model_image=None):
        if model_image is None:
            model_image = self.model_image
        parameters = self.parameter_pointer(parameters)
        return lib.i3_model_likelihood(self, model_image, parameters, dataset)


    def starting_params(self, options):
        p = lib.i3_model_option_starts(self, options)
        p0 = ct.cast(p, self._param_struct_p).contents
        return p0

    def prior(self, parameters):
        parameters = self.parameter_pointer(parameters)
        p = lib.i3_model_prior(self, parameters)
        return p

    @staticmethod
    def parameter_pointer(parameters):
        if isinstance(parameters, structs.ParameterSet_struct):
            parameters = ct.byref(parameters)
        parameters = ct.cast(parameters, structs.ParameterSet_struct_p)
        return parameters


    def prior_violations(self, parameters):
        parameters = parameter_pointer(parameters)
        return lib.i3_model_prior_violations(self, parameters, structs.c_stdout)

    number_varied_params = lib.i3_model_number_varied_params

    def input_varied_params(self, vector, params, baseline):
        n = self.number_varied_params()
        array = (c_i3_flt * n) (*vector)
        baseline = self.parameter_pointer(baseline)
        params = self.parameter_pointer(params)
        return lib.i3_model_input_varied_parameters(self, array, params, baseline)

    def extract_varied_params(self, parameters):
        if isinstance(parameters, structs.ParameterSet_struct):
            parameters = ct.byref(parameters)
        parameters = ct.cast(parameters, structs.ParameterSet_struct_p)
        n = self.number_varied_params()
        array = (c_i3_flt * n)()
        array_cast = ct.cast(array,c_i3_flt_p)
        lib.i3_model_extract_varied_parameters(self, parameters, array_cast)
        return list(array[0:n])

