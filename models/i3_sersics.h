#ifndef _H_I3_SERSICS
#define _H_I3_SERSICS
#include "i3_sersics_definition.h"
#include "i3_image.h"
#include "i3_data_set.h"
#include "i3_options.h"

//i3_flt i3_sersics_likelihood_old(i3_image * model_image, i3_sersics_parameter_set * p, i3_data_set * dataset);

//void i3_sersics_components(i3_sersics_parameter_set * p, i3_data_set * dataset, i3_image * image_dvc, i3_image * image_exp);

void i3_sersics_model_image_with_moffat(i3_sersics_parameter_set * p, i3_options * options, i3_moffat_psf * psf_func, i3_image * model_image);

void i3_sersics_model_image_with_psf_image(i3_sersics_parameter_set * p, i3_options * options, i3_image * psf_image, i3_image * model_image);

void i3_sersics_model_image_using_hankel_transform(i3_sersics_parameter_set * p, i3_data_set * dataset, i3_image * model_image, bool save_components);
void i3_sersics_image(i3_sersics_parameter_set * p, i3_data_set * dataset, i3_image * model_image, i3_flt A, int proc_id, int verb);

void i3_sersics_model_jacobian(i3_sersics_parameter_set * p, i3_data_set * dataset, i3_flt * jac);

void i3_sersics_model_jacobian_exact(i3_sersics_parameter_set * p, i3_data_set * dataset, i3_flt * jac);

void i3_sersics_model_jacobian_exact_vec(i3_sersics_parameter_set * p, i3_data_set * dataset, i3_flt * jac);

void i3_sersics_start(i3_sersics_parameter_set * start, i3_data_set * data_set, i3_options * options);

i3_flt i3_sersics_likelihood(i3_image * model_image, i3_sersics_parameter_set * parameters, i3_data_set * dataset);

void i3_sersics_model_image(i3_sersics_parameter_set * p, i3_data_set * dataset, i3_image * model_image);
void i3_sersics_model_image_save_components(i3_sersics_parameter_set * p, i3_data_set * dataset, i3_image * model_image, bool save_components);

i3_sersics_parameter_set * i3_sersics_beermat_params(i3_sersics_parameter_set * p , i3_options * options);

void i3_sersics_beermat_mapping(i3_sersics_parameter_set * input, i3_sersics_parameter_set * output, i3_options * options);


void i3_sersics_get_image_info(i3_sersics_parameter_set * params, i3_data_set * dataset,  i3_flt * image_args, i3_flt * image_info);

#endif
