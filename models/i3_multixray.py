name="multixray"

parameters = ['alpha_arcsec','delta_arcsec','e1','e2','rc', 'A', 'beta', 'background']
fixed_by_default = ['background']

pi=3.141593

max_exposures = 30

types = {
	'alpha_arcsec':float,
	'delta_arcsec':float,
	'e1':float,
	'e2':float,
	'rt':float,
	'rc':float,
	'A':float,
	'beta':float,
	'background':list,
}

widths = {
	'alpha_arcsec':0.1,
	'delta_arcsec':0.1,
	'e1': 0.1, 
	'e2': 0.2,
	'rt':0.1,
	'rc':0.1,
	'A': 0.3,
	'beta': 0.3,
	'background': [0.1 for i in xrange(max_exposures)],
}

starts = {
	'alpha_arcsec':0. ,
	'delta_arcsec':0. ,
	'e1' : 0.0, 
	'e2': 0.0,
	'rt': 20.0,
	'rc': 2.0,
	'A': 1.0,
	'beta': 2.0/3.,
	'background': [0.0 for i in xrange(max_exposures)],
	
}

# The ones below this point are optional, but encouraged

min = {
	'alpha_arcsec': -20.0,
	'delta_arcsec': -20.0,
	'e1': -0.95,
	'e2': -0.95,
	'rt': 0.2,
	'rc': 0.1,
	'A': 0.,
	'beta': 1.0/6.0,
	'background': [0.0 for i in xrange(max_exposures)],
	
}

max = {
	'alpha_arcsec': 60.0,
	'delta_arcsec': 60.0,
	'e1': 0.95, 
	'e2': 0.95,
	'rt': 100.,
	'rc': 50,
	'A': 10.,
	'beta': 3.0,
	'background': [10.0 for i in xrange(max_exposures)],
}


help = {
	'alpha_arcsec': 'Tangent plane RA offset of centroid from baseline',
	'delta_arcsec': 'Tangent plane DEC offset of centroid from baseline',
	'e1': 'The ellipticity along x axs', 
	'e2': 'The ellipticity along x=y axs',
	'rt': 'The (transitition?) radius r_t',
	'rc':'The outer radius r_c',
	'A': 'Amplitude of the profile',	
	'background': 'A background noise level to add to the model',	
}

proposal = "i3_covariance_matrix_proposal"
# start_function = "i3_multixray_start"
# get_model_image_function = "i3_multixray_model_image"
map_physical_function = "i3_multixray_map"


