name="multisersics"

parameters = ['ra_as','dec_as','e1','e2','radius','radius_ratio','bulge_A','disc_A','bulge_index','disc_index']
fixed_by_default = ['radius_ratio','bulge_index','disc_index']

pi=3.141593

types = {
	'ra_as':float,
	'dec_as':float,
	'e1':float,
	'e2':float,
	'radius':float,
	'radius_ratio':float,
	'bulge_A':float,
	'disc_A':float,
	'bulge_index':float,
	'disc_index':float,
	'delta_e_bulge':float,
	'delta_theta_bulge':float,
}

widths = {
	'ra_as':0.1,
	'dec_as':0.1,
	'e1': 0.1, 
	'e2': 0.2,
	'radius':0.1,
	'radius_ratio':0.1,
	'bulge_A': 0.3,
	'disc_A':  0.2,
	'bulge_index':0.2,
	'disc_index':0.2,
	'delta_e_bulge':0.1,
	'delta_theta_bulge':0.1,
}

starts = {
	'ra_as':0. ,
	'dec_as':0. ,
	'e1' : 0.0, 
	'e2': 0.0,
	'radius': 2.7,
	'radius_ratio': 1.,
	'bulge_A': 0.5,
	'disc_A': 0.5,
	'bulge_index':4.0,
	'disc_index':1.0,
	'delta_e_bulge':0.0,
	'delta_theta_bulge':0.0,
	
}

# The ones below this point are optional, but encouraged

min = {
	'ra_as': -20.0,
	'dec_as': -20.0,
	'e1': -0.95,
	'e2': -0.95,
	'radius': 0.1,
	'radius_ratio': 0.001,
	'bulge_A': 0.,
	'disc_A': 0.,
	'bulge_index':0.5,
	'disc_index':0.5,
	'delta_e_bulge':-0.5,
	'delta_theta_bulge':-pi,
	
}

max = {
	'ra_as': 20.0,
	'dec_as': 20.0,
	'e1': 0.95, 
	'e2': 0.95,
	'radius': 50.,
	'radius_ratio': 1000,
	'bulge_A': 10000.,
	'disc_A': 10000.,
	'bulge_index':5.0,
	'disc_index':5.0,
	'delta_e_bulge':0.5,
	'delta_theta_bulge':pi,
}


help = {
	'ra_as': 'The x-coordinate of the center of the de Vaucouleurs bulge profile',
	'dec_as': 'The y-coordinate of the center of the de Vaucouleurs bulge profile',
	'e1': 'The ellipticity along x axs', 
	'e2': 'The ellipticity along x=y axs',
	'radius': 'The bulge scale radius (exp scale radius is set from this too)',
	'radius_ratio':'The bulge-disc ratio',
	'bulge_A': 'amplitude of the bulge',
	'disc_A': 'amplitude of the disc',
	'bulge_index':'Sersic index of the bulge component',
	'disc_index':'Sersic index of the disc component',
	'delta_e_bulge':'The additional e in the bulge component compared to the disc',
	'delta_theta_bulge':'The rotation of the the bulge compared to the disc',
	
}

proposal = "i3_covariance_matrix_proposal"
start_function = "i3_multisersics_start"
#get_model_image_function = "i3_multisersics_model_image"
map_physical_function = "i3_multisersics_beermat_mapping"

