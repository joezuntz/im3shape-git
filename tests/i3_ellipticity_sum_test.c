#include "i3_image.h"

int main(){

	i3_flt e1 = 0.3;
	i3_flt e2 = 0.2;
	i3_flt t1, t2;
	
	i3_ellipticity_sum(e1, e2, 0.0, 0.0, &t1, &t2);
	printf("Should be (0.3, 0.2):  (%f,%f)\n", t1, t2);

	i3_ellipticity_sum(e1, e2, 0.01, 0.0, &t1, &t2);
	printf("Should be a close to (0.3, 0.2):  (%f,%f)\n", t1, t2);

	i3_ellipticity_sum(e1, e2, 0.0, 0.01, &t1, &t2);
	printf("Should be a close to (0.3, 0.2):  (%f,%f)\n", t1, t2);

	i3_ellipticity_sum(e1, 0.0, 0.01, 0.0, &t1, &t2);
	printf("Should be close to (0.31, 0.0):  (%f,%f)\n", t1, t2);

	i3_ellipticity_sum(0.0, e2, 0.0, 0.01, &t1, &t2);
	printf("Should be close to (0.0, 0.21):  (%f,%f)\n", t1, t2);

	
	return 0;
}