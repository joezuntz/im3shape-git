% here we check what is the difference between base image and image padded with zeros on
% the edges and. Or - in other words - between high number of pixels and
% low number of pixels. Essentially we are checking for aliasing and convolution edge effects. 

clc; clear all;



%% load stuff

load image_gal_l.txt	
load image_pix_l.txt
load image_psf_l.txt
load image_obs_l.txt
load image_obs_s.txt
load image_obs_l_cut.txt

%% get the difference signifficance
max(abs(image_obs_l_cut(:)-image_obs_s(:)))/max(image_obs_s(:))

%% plots 

n=0;
n=n+1;figure(n);imagesc(image_gal_l);colorbar;axis equal; title('image gal l')
n=n+1;figure(n);imagesc(image_pix_l);colorbar;axis equal; title('image pix l')
n=n+1;figure(n);imagesc(image_psf_l);colorbar;axis equal; title('image psf l')
n=n+1;figure(n);imagesc(image_obs_l);colorbar;axis equal; title('image obs l')
n=n+1;figure(n);imagesc(image_obs_s);colorbar;axis equal; title('image obs s')
n=n+1;figure(n);imagesc(image_obs_l_cut);colorbar;axis equal; title('image obs l cut')
n=n+1;figure(n);imagesc(image_obs_l_cut-image_obs_s);colorbar;axis equal; title('image obs l cut')


