#include "i3_image.h"
#include "i3_model_tools.h"
#include "i3_model.h"
#include "i3_image_fits.h"
#include "i3_data_set.h"
#include "i3_analyze.h"

int main(){
	i3_fftw_load_wisdom();
	atexit(i3_fftw_save_wisdom);
	i3_init_rng();
	
	int n = 96;
	i3_image * image = i3_image_create(n,n);
	
	// i3_flt ab = 25;
	// i3_flt e1 = 0.0;
	// i3_flt e2 = 0.9;
	// i3_flt A = 1.0;
	// i3_flt x0 = n/2.;
	// i3_flt y0 = n/2.;
	
	
	i3_options * options = i3_options_default();
	i3_model * model = i3_model_create("sersics",options);
	i3_sersics_parameter_set * p = i3_model_option_starts(model, options);
	p->radius = 20.0;
	p->x0 = n/2.0;
	p->y0 = n/2.0;
	p->e1 = 0.3;
	p->radius_ratio = 1.0;

	i3_moffat_psf * psf = i3_small_round_psf();
	i3_data_set * dataset = i3_build_dataset(options, image, NULL, psf, GREAT10_PSF_TYPE_MOFFAT);


	p->bulge_A = 0.0;
	p->disc_A = 1.0;
	p->delta_e_bulge = 0.0;
	i3_sersics_model_image(p, dataset, image);
	i3_image_save_fits(image,"round_1.fits");

	p->bulge_A = 1.0;
	p->disc_A = 0.0;
	p->delta_e_bulge = 0.0;
	i3_sersics_model_image(p, dataset, image);
	i3_image_save_fits(image,"round_2.fits");


	p->bulge_A = 1.0;
	p->disc_A = 0.0;
	p->delta_e_bulge = 0.3;
	p->delta_theta_bulge = M_PI/2;
	
	i3_sersics_model_image(p, dataset, image);
	i3_image_save_fits(image,"squidge_1.fits");

	p->bulge_A = 0.0;
	p->disc_A = 1.0;
	p->e1 = 0.0;
	p->delta_e_bulge = 0.3;
	p->delta_theta_bulge = M_PI/2;
	i3_sersics_model_image(p, dataset, image);
	i3_image_save_fits(image,"squidge_2.fits");


	p->bulge_A = 1.0;
	p->disc_A = 1.0;
	p->e1 = 0.2;
	p->delta_e_bulge = -0.2;
	p->delta_theta_bulge = M_PI/2;
	
	i3_sersics_model_image(p, dataset, image);
	i3_image_save_fits(image,"mix_1.fits");
	
	
	p->bulge_A = 1.0;
	p->disc_A = 1.0;
	p->e1 = 0.2;
	p->delta_e_bulge = 0.0;
	p->delta_theta_bulge = M_PI/2;
	
	i3_sersics_model_image(p, dataset, image);
	i3_image_save_fits(image,"mix_2.fits");
	

}