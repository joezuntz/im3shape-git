#include "i3_wcs.h"
#include "i3_wcstools.h"

void i3_print_transform(i3_transform T){
	printf("ra_0 = %.8le\n",T.ra0);
	printf("dec_0 = %.8le\n",T.dec0);
	printf("x_0 = %le\n",T.x0);
	printf("y_0 = %le\n",T.y0);
	printf("A = {% .3le    % .3le}\n",T.A[0][0],T.A[0][1]);
	printf("    {% .3le    % .3le}\n",T.A[1][0],T.A[1][1]);
}

int main(int argc, char * argv[]){

  char * filename = argv[1];
  // int hdu = atoi(argv[2]);
  int verbose = 1;
  // struct wcsprm * wcs = i3_get_wcsprm_from_file(filename, hdu);
  struct WorldCoor * wcs = NULL;
  wcs = GetWCSFITS(filename, verbose);
  if (wcs==NULL){
    fprintf(stderr, "Failed to get WCS from header of %s\n",filename);
    return 1;
  }

  i3_flt x0 = 10.;
  i3_flt y0 = 10.;

  // i3_flt ra0 = 16.003;    // Central RA and DEC from DECam_00139784_62 r-band image
  // i3_flt dec0 = -31.034;

  // i3_flt e1_image_test = 0.1;
  // i3_flt e2_image_test = -0.44;
  // i3_flt ab_image_test = 7.7;
  // i3_flt x_image_test = -11375.2; // almost centered, not quite
  // i3_flt y_image_test = -2211.333;

  // Ready variables for storing return values of params in wcs coords
  // i3_flt e1_wcs_ret, e2_wcs_ret, ab_wcs_ret, ra_image_ret, dec_image_ret;

// i3_transform trans = i3_get_image_to_wcs_transform(x_image_test, y_image_test, wcs);
  // i3_transform trans = i3_get_wcs_to_image_transform(ra0, dec0, wcs);

  i3_transform trans2 = i3_get_image_to_wcs_transform(x0, y0, wcs);



printf("Made transform okay\n");
i3_print_transform(trans2);
return 0;
}
