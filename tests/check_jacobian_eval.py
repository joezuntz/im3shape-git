from numpy import *

exact  = loadtxt('nbc_result_000066_000066_exact.dat')
approx = loadtxt('nbc_result_000066_000066_approx.dat')                 

# column in python 
# 20 error at starting point
# 21 error at estimate
# 28 function evaluations
# 29 Jacobian evaluations



print mean((exact[:,21]-approx[:,21])/approx[:,21])


