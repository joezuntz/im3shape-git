#include "stdio.h"
#include "stdlib.h"
#include "lbfgsb.h"
#include "string.h"
#include "math.h"
#include "i3_minimizer.h"


int main(int argc, char * argv[]){
	i3_options * options = i3_options_default();
	options->minimizer_tolerance = atof(argv[1]);
	options->minimizer_verbosity = 3;
	i3_model * model = i3_model_create("quadratic_test",options);
	if (!model){
		I3_FATAL("You must compile with support for the quadratic_test model to use lbf_test",1);
	}
	i3_minimizer * minimizer = i3_minimizer_create(model,options);

	i3_data_set * data_set = NULL;
	i3_parameter_set * initial = i3_model_option_starts(model, options);
	i3_parameter_set * result = i3_minimizer_run_lbf(minimizer,initial,data_set);
	i3_model_pretty_printf_parameters(model,result); printf("\n");
}