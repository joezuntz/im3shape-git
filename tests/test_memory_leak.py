"""
This script tests memory usage for the im3shape.py driver.  Run it
with the profiler of your choice.I found memory_profiler a good
choice. If you don't have it installed, use pip to get it:

sudo pip install -U memory_profile

Then run test_memory_leak.py as:
python -m memory_profiler test_memory_leak.py

Make sure you have im3shape.py in your PYTHONPATH by
export PYTHONPATH=<full-path-to-im3shape.py>
"""

@profile
def main(argv):
    """
    Script to test memory usage of im3shape.py
    """
    import im3shape as i3
    import numpy as np 

    # Create i3_image of certain stamp size
    stamp_size = 500
    
    i3_galaxy = i3.I3_image(stamp_size, stamp_size)
    del i3_galaxy
    
    for i in range(1):
        i3_galaxy = i3.I3_image(stamp_size, stamp_size)

    for i in range(10):
        i3_galaxy = i3.I3_image(stamp_size, stamp_size)

    for i in range(100):
        i3_galaxy = i3.I3_image(stamp_size, stamp_size)

    tmp = np.ones(stamp_size)
    del tmp

    for i in range(1):
        tmp = np.ones(stamp_size)    

    for i in range(10):
        tmp = np.ones(stamp_size)
        
    for i in range(100):
        tmp = np.ones(stamp_size)      

if __name__ == "__main__":
    main(sys.argv)
