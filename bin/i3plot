#!/usr/bin/env python
try:
	import pyfits
except ImportError:
	import astropy.io.fits as pyfits
import glob
import os
import sys
import numpy as np

def find_indices():
	indices = [f.split('.')[1] for f in glob.glob('image.*.fits')]
	return [int(i) for i in indices if os.path.exists("model.%s.fits"%i)]

def triplot_all():
	for index in find_indices():
		triplot_file(index)

def triplot_file(index, save=None, psf=False, weight=False, cut=False):
	image_filename = 'image.%d.fits'%index
	model_filename = 'model.%d.fits'%index
	psf_filename = 'psf.%d.fits'%index
	weight_filename = 'weight.%d.fits'%index
	image = pyfits.getdata(image_filename)
	model = pyfits.getdata(model_filename)
	if psf: psf = pyfits.getdata(psf_filename)
	else: psf = None
	if weight: weight = pyfits.getdata(weight_filename)
	else: weight = None

	if cut and (weight is not None):
		image[weight<=0] = np.nan
		model[weight<=0] = np.nan

	return triplot(image, model, psf=psf, weight=weight, save=save)


def triplot(image, model, psf=None, weight=None, save=None):
	import pylab
	residual = model-image
	ny, nx = image.shape
	nplot = 3
	if psf is not None:
		nplot+=1
	if weight is not None:
		nplot+=1
	width = 3.*(nx+0.5*ny)/ny
	height = nplot*3

	f = pylab.figure(figsize=(width, height))
	pylab.subplot(nplot,1,1)
	pylab.imshow(image, interpolation='nearest')
	pylab.title("Image")
	pylab.colorbar()
	
	pylab.subplot(nplot,1,2)
	pylab.imshow(model, interpolation='nearest')
	pylab.title("Model")
	pylab.colorbar()
	
	pylab.subplot(nplot,1,3)
	pylab.imshow(residual, interpolation='nearest')
	pylab.title("Residual")
	pylab.colorbar()

	if psf is not None:
		pylab.subplot(nplot,1,4)
		pylab.imshow(psf, interpolation='nearest')
		pylab.title("PSF")
		pylab.colorbar()

	if weight is not None:
		pylab.subplot(nplot,1,nplot)
		pylab.imshow(weight, interpolation='nearest')
		pylab.title("Weight")
		pylab.colorbar()


	if save:
		pylab.savefig(save)
		pylab.close()
	return f	

import argparse
parser=argparse.ArgumentParser("Generate model/image/residual plots")
parser.add_argument("-n", "--index", type=int, default=None, help="Index of object to plot, if not all")
parser.add_argument("-w", "--weight", action='store_true', help="Include weight on plots")
parser.add_argument("-p", "--psf", action='store_true', help="Include PSF on plots")
parser.add_argument("-c", "--cut", action='store_true', help="Cut out zero-weighted parts of the image and model plots")


if __name__ == '__main__':
	import matplotlib
	args = parser.parse_args()
	matplotlib.use("Agg")
	for index in find_indices():
		if (args.index is not None) and (index!=args.index): continue
		print index
		triplot_file(index,save='%d.png'%index, psf=args.psf, weight=args.weight, cut=args.cut)
