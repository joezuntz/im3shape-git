#ifndef _H_I3_IMAGE_FITS
#define _H_I3_IMAGE_FITS

#include "i3_image.h"
#include <fitsio.h>

int i3_image_save_fits(i3_image * image, char * filename);
int i3_fits_image_copy_part_into(fitsfile * f, i3_image * image, int x0, int y0);
i3_image * i3_fits_image_copy_part(fitsfile * f, int x0, int y0, int width, int height);




#endif 


