#ifndef _H_I3_POWELL
#define _H_I3_POWELL

typedef float (*powell_function)(float *p, void * args);
#define INNER_ITERATIONS 100
#define N_PARAMS 4


int i3_powell_minimization(powell_function f, float * start_pos, float * param_min, float * param_max, void * args, int n, int max_it, float ftol, float * result, float * min_pos);



#endif



