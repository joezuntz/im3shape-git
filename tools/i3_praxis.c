#include "i3_praxis.h"
double praxis_ ( double * tolerance, double * max_step_size, int * n, int * print_options, double * x, praxis_function f , long int * args, int *, int *, int *, int *);



typedef struct praxis_parameters{
	double * minima;
	double * maxima;
	praxis_function function;
	void * args;
	int n;
} praxis_parameters;


void i3_praxis_scale_to_unit(int n, double * minima, double * maxima, double * unscaled, double * scaled)
{
	for (int p=0;p<n;p++){
		double range = maxima[p]-minima[p];
		scaled[p] = (unscaled[p]-minima[p])/range;
	}
}



void i3_praxis_scale_from_unit(int n, double * minima, double * maxima, double * scaled, double * unscaled){
	for (int p=0;p<n;p++){
		double range = maxima[p]-minima[p];
		unscaled[p] = scaled[p]*range + minima[p];
	}
}



double i3_praxis_wrapper(double * x, void * params){
	praxis_parameters * parameters = (praxis_parameters*)params;
	void * args = parameters->args;
	double unscaled_parameters[parameters->n];
	i3_praxis_scale_from_unit(parameters->n, parameters->minima, parameters->maxima, x, unscaled_parameters);
	double y = parameters->function(unscaled_parameters,args);
	return y;
	
}

int i3_praxis_minimization(praxis_function f, double * start_position, int max_iterations, double * minima, double * maxima, void * args, int n, double tolerance, double * minimum_value, double * minimum_location, int verbosity)
{
	
	praxis_parameters P;
	P.minima = minima;
	P.maxima = maxima;
	P.function = f;
	P.args = args;
	P.n = n;
	double step_size = 0.5;
	double x[n];
	i3_praxis_scale_to_unit(n, minima, maxima, start_position, x);
	
	
	int print_options = (verbosity>=0) ? verbosity : 0;
	print_options = (verbosity<=4) ? verbosity : 4;
	
	int ill_conditioned = 0;
	int badly_scaled = 1;
	int runs_for_termination = 3;
	
	*minimum_value = praxis_ (&tolerance, &step_size, &n, &print_options, x,&i3_praxis_wrapper, (long int*)&P, &max_iterations, &ill_conditioned, &badly_scaled, &runs_for_termination);
	i3_praxis_scale_from_unit(n, minima, maxima, x, minimum_location);
	return 0;
	
}

