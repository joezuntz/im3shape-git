#include "i3_intel_mkl_random.h"

#define RNG_BUFFER_SIZE 1024

typedef struct RNG_Buffer{
	void * RNG;
	double buffer_uniform_double[RNG_BUFFER_SIZE];
	float buffer_uniform_float[RNG_BUFFER_SIZE];
	double buffer_normal_double[RNG_BUFFER_SIZE];
	float buffer_normal_float[RNG_BUFFER_SIZE];
	int u_double;
	int u_float;
	int n_double;
	int n_float;
} RNG_Buffer;

RNG_Buffer rng_buffer;


void i3_intel_random_buffer_uniform_double(){
	int status = vdRngUniform(VSL_METHOD_DUNIFORM_STD,rng_buffer.RNG,RNG_BUFFER_SIZE, rng_buffer.buffer_uniform_double, 0.0, 1.0 );
	rng_buffer.u_double=0;	
}

void i3_intel_random_buffer_uniform_float(){
	int status = vsRngUniform(VSL_METHOD_SUNIFORM_STD,rng_buffer.RNG,RNG_BUFFER_SIZE, rng_buffer.buffer_uniform_float, 0.0, 1.0 );
	rng_buffer.u_float=0;
}

void i3_intel_random_buffer_normal_double(){
	int status = vdrnggaussian(VSL_METHOD_DGAUSSIAN_BOXMULLER2,rng_buffer.RNG,RNG_BUFFER_SIZE,rng_buffer.buffer_normal_double,0.0,1.0);
	rng_buffer.n_double=0;
}

void i3_intel_random_buffer_normal_float(){
	int status = vsrnggaussian(VSL_METHOD_SGAUSSIAN_BOXMULLER2,rng_buffer.RNG,RNG_BUFFER_SIZE,rng_buffer.buffer_normal_float,0.0,1.0);
	rng_buffer.n_float=0;
}



void i3_intel_init_rng(){
	int status = vslnewstream(rng_buffer.RNG,VSL_BRNG_MT19937,1231321);
	i3_intel_random_buffer_uniform_double();
	i3_intel_random_buffer_uniform_float();
	i3_intel_random_buffer_normal_double();
	i3_intel_random_buffer_normal_float();
}

void i3_intel_init_rng_multiprocess(int seed){
	int status = vslnewstream(rng_buffer.RNG,VSL_BRNG_MT19937,seed);
	i3_intel_random_buffer_uniform_double();
	i3_intel_random_buffer_uniform_float();
	i3_intel_random_buffer_normal_double();
	i3_intel_random_buffer_normal_float();
}


float i3_intel_random_uniform_float(){
	if (rng_buffer.u_float==RNG_BUFFER_SIZE) i3_intel_random_buffer_uniform_float();
	return rng_buffer.buffer_uniform_float[rng_buffer.u_float++];
}

double i3_intel_random_uniform_double(){
	if (rng_buffer.u_double==RNG_BUFFER_SIZE) i3_intel_random_buffer_uniform_double();
	return rng_buffer.buffer_uniform_double[rng_buffer.u_double++];
}

float i3_intel_random_normal_float(){
	if (rng_buffer.n_float==RNG_BUFFER_SIZE) i3_intel_random_buffer_normal_float();
	return rng_buffer.buffer_normal_float[rng_buffer.n_float++];
}

double i3_intel_random_normal_double(){
	if (rng_buffer.n_double==RNG_BUFFER_SIZE) i3_intel_random_buffer_normal_double();
	return rng_buffer.buffer_normal_double[rng_buffer.n_double++];
}

